package com.example.booksapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class BooksapiApplication {

  public static void main(String[] args) {
    SpringApplication.run(BooksapiApplication.class, args);
  }

  @GetMapping("/getAll")
  public String[] getProducts(){
    return new String[]{"book 1", "book 2", "book 3"};
  }

}
